# Dockerfile

# Copyright 2019 Ryan Ross nazufel@gmail.com

FROM golang:1.8-alpine

# make the app directory for binary
RUN mkdir /app 

# add source files into the directory
ADD . /app/ 

# assign the /app dir as the working directory
WORKDIR /app 

# build the binary in the /app dir and name it main
RUN go build -o main . 

# run the main app binary when the container starts
CMD ["/app/main"]