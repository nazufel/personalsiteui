# Personal Website

## About

Front end website UI for perosnal website. Served up with Golang [http.FileServer](https://golang.org/pkg/net/http/#FileServer) to serve up the static files.

### Atribution

I suck at front end and pulled this tempalte from [Black Rock Creative LLC](https://github.com/BlackrockDigital/startbootstrap-creative).

## Copyright and License

Copyright 2019 Ryan Ross. Code released under the [MIT](https://gitlab.com/nazufel/personalsiteui/blob/master/LICENSE) license.
