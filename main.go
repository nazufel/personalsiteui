package main

import (
	"log"
	"net/http"
	"strings"
)

func main() {
	// serve up everything in the current directory
	http.Handle("/", http.StripPrefix(strings.TrimRight("/", "/"), http.FileServer(http.Dir("."))))

	// listen and serve on port 8000 using the default mux.
	log.Fatal(http.ListenAndServe(":8000", nil))
}
