# Makefile

## Makefile to make my life easier

## MAINTAINER: Ryan Ross nazufel@gmail.com

# build the Docker image from Dockerfile
dbuild:
	$(clean_command)
	docker build -t ui .

# run the latest ui image and name it ui
drun:
	$(clean_command)
	docker run -d --name ui -p 8000:8000 ui

# stop the running ui container
dstop:
	$(clean_command)
	docker rm ui -f

# stop all running containers
dstopall:
	$(clean_command)
	docker rm $(docker ps -aq)

# run the webserver locally

run:
	${clean_command}
	go build main.go; ./main